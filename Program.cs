﻿using System;

namespace LoopsDemo
{
    internal class Program
    {
        private static void Main()
        {
            const Int32 stopValue = 10;

            #region goto-loop

            var counter = 0;
            label:
            Console.WriteLine(counter);
            counter++;
            if (counter < stopValue)
                goto label;

            #endregion

            #region infinity-loop

            var index = 0;
            while (true)
            {
                Console.WriteLine(index);
                index++;
                if (index >= stopValue)
                    break;
            }

            #endregion

            const Int32 magicNumber = 7;
            for (var i = 0; i < stopValue; i++)
                for (var j = 0; j < stopValue; j++)
                    if (i + j == magicNumber)
                        goto exit;
            exit:
            Console.WriteLine("exit");
        }
    }
}
